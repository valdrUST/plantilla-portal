#!/bin/bash
#mando a llamar jwt.sh para generar el JWT que identificara al servidor con el servicio de analytics y obtener el TOKEN
jwt=$(sh jwt.sh)
#Obtengo el token de identificacion generado con el JWT por medio de la URL
result=$(curl -s  -m 60 --data-urlencode grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer --data-urlencode assertion=$jwt https://www.googleapis.com/oauth2/v4/token)
accessToken=$(echo $result | ./jq -c '.access_token' | sed 's/"//g')
startDate="2018-03-03"
endDdate="today"
profileID="176366157"
#este parametro es el que me da las visitas unicas a la pagina
desiredMetric="UniquePageViews"
today=$(date "+%Y-%m-%d")
#Obtengo los datos de la API de analytics
regreso=$(curl -s "https://www.googleapis.com/analytics/v3/data/ga?ids=ga:$profileID&metrics=ga:$desiredMetric&start-date=$startDate&end-date=$today&access_token=$accessToken&Accept-Encoding=gzip&User-Agent=my program (Gzip)")
echo $regreso |  tr , '\n' | grep "totalsForAllResults" | cut -d'"' -f6 > visitas.txt