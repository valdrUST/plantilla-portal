$(document).ready(function(){

    /* Botón ir Arriba */

                $('.gotoTop').click(function(){
                    $('body, html').animate({
                        scrollTop: '0px'
                    }, 300);
                });
 
                $(window).scroll(function(){
                    if( $(this).scrollTop() > 100 ){
                        $('.gotoTop').slideDown(300);
                    } else {
                        $('.gotoTop').slideUp(300);
                    };
                });
})