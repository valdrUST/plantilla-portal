#!/bin/bash
#se filtra el texto de los tweets que retorna la api 
sh curlGETtweets.sh > twits.json
cat twits.json | ./jq -c '.[] | .text' | grep "#SAviso" | sed 's/#SAviso //g' > texto.txt
cat twits.json | ./jq -c '.[] | .text +" " + .entities.media[0].media_url' | grep "#SAviso" | sed 's/#SAviso //g' | sed 's/\n//g' > texto.txt
#se genera una array con las lineas de texto
mapfile -t lineas < texto.txt
#este crea los archivos temporales que se usaran para consultar los datos
touch eliminacion
touch mensajes
rm mensajes
touch mensajes
#recorre linea a linea el texto que se filtro
for linea in "${lineas[@]}"
do
	eliminarse=$(echo $linea | grep -o "#o[[:digit:]]*" | sed 's/#o//g')
	if [[ $eliminarse != "" ]]; then
		echo $eliminarse >> eliminacion #este guarda los ids que aparecen en la #o que es el texto que se va a filtrar
	fi
done
#se crea un array con los ids de los mensajes que se filtraran
mapfile -t eliminarse < eliminacion
#este recorre las lineas de texto para filtar los mensajes que seran filtrados por fecha de expiracion
for linea in "${lineas[@]}"; do
	identificador=$(echo $linea | grep -o "#f[[:digit:]]*" | sed 's/#f//g')
	fechaCorte=$(echo $linea | grep -o " #t[[:digit:]]*" | sed 's/#t//g')
	if [[ $identificador != ""  && ${#fechaCorte} = 11 ]]; then #en la fecha corte se recupera su longitud que exista y sea igual a 11 para el formato yymmddhhMM\n
		for lineaRefresh in "${lineas[@]}"; do #esta iteracion busca los refresh de las fechas de expiracion
			refreshId=$(echo $lineaRefresh | grep -o "#r$identificador" | sed 's/#r//g')
			if [[ $refreshId != "" ]]; then
				refreshFechaCorte=$(echo $lineaRefresh | grep -o "#t[[:digit:]]*" | sed 's/#t//g')
				linea=$(echo $linea | sed 's/#t[[:digit:]]*/#t'"$refreshFechaCorte"'/g')
				fechaCorte=$refreshFechaCorte
				break
			fi
		done
		fechaActual=$(date +%y%m%d%H%M)
		if [[ $fechaActual < $fechaCorte ]]; then
			busqueda=$( cat mensajes | grep "$linea" )
			echo $linea >> mensajes
		fi
		if [[ $fechaCorte = "" ]]; then
			echo $linea >> mensajes
		fi
	fi
done
rm eliminacion
cat mensajes | sort | uniq > mensajes2
rm mensajes
mapfile -t lineas < mensajes2
for linea in "${lineas[@]}"; do
	for del in "${eliminarse[@]}"; do
		elim=$(echo $linea | grep -o "$del") 
		if [[ $elim != "" ]]; then
			sed -i /"$linea"/d mensajes2
		fi
	done
done
#elimino primer y ultimo caracter (eliminar las "" de la linea)
sed -i 's/^.\|.$//g' mensajes2

#elimina los hashtags que no interesan en el mensaje final

#sed -i s/"#t[0-9]* "//g mensajes2
sed -i s/"#o[0-9]* "//g mensajes2

mapfile -t avisos < mensajes2
echo '{"avisos":[' > carrusel.json 
for aviso in "${avisos[@]}"; do
	id=$(echo $aviso | grep -o '#f[[:digit:]]*' | sed 's/#f//g')
	echo '{"id":"'$id'",' >> carrusel.json
	imagen=$(echo $aviso | grep -o 'http:\/\/pbs.twimg.com\/media\/[[:alnum:]|[:punct:]]*')
	aviso=$(echo $aviso | sed s/"#f[[:digit:]]* "//g)
	aviso=$(echo $aviso | sed s/"https:\/\/t.co\/[[:alnum:]]* http:\/\/pbs.twimg.com\/media\/[[:alnum:]|[:punct:]]*"//g )
	fechaCorte=$(echo $aviso | grep -o "#t[[:digit:]]*" | sed 's/#t//g')
	aviso=$(echo $aviso | sed s/"#t[[:digit:]]* "//g)
	aviso=$(echo $aviso | sed s/"#t[[:digit:]]*"//g)
	echo '"texto":"'$aviso'"' >> carrusel.json
	echo ',"fechaCorte":"'$fechaCorte'"' >> carrusel.json
	if [[ $imagen != "" ]]; then
		echo ',"urlImg":"'$imagen'"},' >> carrusel.json
	else
		echo '},'>>carrusel.json
	fi
done
echo '{' >> carrusel.json
echo '"id":"00000000"' >> carrusel.json
echo '}' >> carrusel.json
echo ']}' >> carrusel.json
cat carrusel.json 
rm mensajes2