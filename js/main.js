/* ACCIONES AL CARGAR LA PÁGINA */
$(document).ready(function(){
    /* CARGA EL ENCABEZADO Y PIE */
    $('#header').load('plantilla/encabezado.html');
    $('#footer').load('plantilla/pie.html');

    /* CARGA EL CARRUSEL CON LOS MENSAJES */
    var request_data = {
		url: 'js/carrusel.json',
        method: 'GET',
        dataType: "json",
		data: { q: '#SAviso' }
	};
	$.ajax({
	  	url: request_data.url,
	  	type: request_data.method
		}).done(function(data) {
            data.avisos.pop();
            prim=data.avisos.pop();
            var txtHTML=$(".carousel-inner").html();
            $(".carousel-inner").html("");
            console.log(prim);
            var htmlElementoActivo = "";
            function dibujar(prim) {
                if(prim){
                    if(txtHTML){
                        txtHTML = txtHTML.replace("carousel-item active", "carousel-item");
                    }
                    if(prim.texto && prim.urlImg){
                        var htmlElementoActivo="<div class=\"carousel-item active\"><div class=\"row\" style=\"width:100%; height:100%;\"><div class=\"col-6\" style=\"width:100px; height: 100px;\"><br><br><p>"+prim.texto+"</p></div><div class=\"col-6\" style=\" height:86%; padding-left:0%; margin-top:3%; margin-bottom:3%;\"><img style=\"height: auto; width: 100%; padding:3%; max-width: 93%; max-height:93%;\" src="+prim.urlImg+"></img></div></div></div>";
                    }else if(prim.texto && !prim.urlImg){
                        var htmlElementoActivo="<div class=\"carousel-item active\"><div style=\" width:100%; height:100%; display: flex; justify-content: center; align-content: center; flex-direction: column;\"><p>"+prim.texto+"</p></div></div>";
                    }else if(!prim.texto && prim.urlImg){
                        var htmlElementoActivo="<div class=\"carousel-item active\"><img style=\"padding:3%; object-fit: contain;\" src="+prim.urlImg+"></img></div>";
                    }                    
                }
                return htmlElementoActivo;
            }
            htmlElementoActivo = dibujar(prim);            
            $(".carousel-inner").html(htmlElementoActivo);
            var numIndicadores=5;
            linea=data.avisos.pop();
            numIndicadores++;
            while(linea){
                numIndicadores++;
                console.log(linea);
                if(linea.texto && linea.urlImg){
                    $(".carousel-inner").append("<div class=\"carousel-item\"><div class=\"row\" style=\"width:100%; height:100%;\"><div class=\"col-6\" style=\"width:100px; height: 100px; \"><br><br><p>"+linea.texto+"</p></div><div class=\"col-6\" style=\" height:90%; margin-top:3%; margin-bottom:3%;\"><img style=\"padding:5% 0%; max-width:95%; object-fit: contain;\" src="+linea.urlImg+"></img></div></div></div>");
                }else if(linea.texto && !linea.urlImg){
                    $(".carousel-inner").append("<div class=\"carousel-item\"><div style=\" width:100%; height:100%; display: flex; justify-content: center; align-content: center; flex-direction: column;\"><p>"+linea.texto+"</p></div></div>");
                }else if(!linea.texto && linea.urlImg){
                    $(".carousel-inner").append("<div class=\"carousel-item\"><img style=\" padding:3%; object-fit: contain;\" src="+linea.urlImg+"></img></div>");
                }
                $(".carousel-indicators").append("<li data-target=\"#carousel-inicio\" data-slide-to=\""+numIndicadores+"\"></li>");
                linea=data.avisos.pop();
                console.log(linea);
            }
            $(".carousel-indicators").append("<li data-target=\"#carousel-inicio\" data-slide-to=\""+numIndicadores+"\"></li>");
            $(".carousel-inner").append(txtHTML);
		});
    
    /* Carga los datos del contador de visitas */
	var request_data_visit = {
		url: 'js/visitas.txt',
		method: 'GET'
		};
	$.ajax({
        async: false,
	  	url: request_data_visit.url,
	  	type: request_data_visit.method
		}).done(function(visit) {
				$(".contador-visitas").append(visit);
        });
    
});