$(document).ready(function(){
    $('#rei').css('display','none');
    $.getJSON("js/indicador.json", function(datos) {
        document.getElementById("j_carrera").innerHTML = datos.datos_generales.carreras;

        document.getElementById("a_total").innerHTML = datos.alumnos.totales;
        if (datos.alumnos.nuevo_ingreso.status) {
            $('#rei').css('display','block');
            document.getElementById("a_nuevo").innerHTML = datos.alumnos.nuevo_ingreso.total;
        }
        document.getElementById("a_reingreso").innerHTML = datos.alumnos.reinscripcion;
        document.getElementById("a_beca").innerHTML = datos.alumnos.alu_beca;
        document.getElementById("a_deportivo").innerHTML = datos.alumnos.alu_deportivo;
        
        document.getElementById("p_total").innerHTML = datos.profesores.totales;
        document.getElementById("p_lic").innerHTML = datos.profesores.g_licenciatura;
        document.getElementById("p_ma").innerHTML = datos.profesores.g_maestria;
        document.getElementById("p_doc").innerHTML = datos.profesores.g_doctorado;
        document.getElementById("p_pos").innerHTML = datos.profesores.g_postdoctorado;
        document.getElementById("p_tec").innerHTML = datos.profesores.g_tecnico;
        document.getElementById("p_bac").innerHTML = datos.profesores.g_bachillerato;
        document.getElementById("p_otro").innerHTML = datos.profesores.g_otro;
        document.getElementById("n_pasi").innerHTML = datos.profesores.n_profesor_asignatura;
        document.getElementById("n_pca").innerHTML = datos.profesores.n_profesor_carrera;
        document.getElementById("n_pin").innerHTML = datos.profesores.n_investigador;
        document.getElementById("n_ta").innerHTML = datos.profesores.n_tecnico_academico;
        document.getElementById("n_ayi").innerHTML = datos.profesores.n_ayudante_investigador;
        document.getElementById("n_ayp").innerHTML = datos.profesores.n_ayudante_profesor;
        document.getElementById("n_sn").innerHTML = datos.profesores.n_sin_nombramiento;

        var nivel = {
            type: "pie",
            data : {
                datasets :[{
                    data : [
                        datos.profesores.pg_postdoctorado,
                        datos.profesores.pg_diplomado,
                        datos.profesores.pg_doctorado,
                        datos.profesores.pg_tecnico,
                        datos.profesores.pg_maestria,
                        datos.profesores.pg_bachillerato,
                        datos.profesores.pg_licenciatura,
                        datos.profesores.pg_otro,
                    ],
                    backgroundColor: [
                        "#F7464A",
                        "#46BFBD",
                        "#FDB45C",
                        "#949FB1",
                        "#4D5360",
                        "#3ADF00",
                        "#3A01DF",
                        "#FE642E",
                    ],
                }],
                labels : [
                    "Postdoc.",
                    "Diplomado",
                    "Doctorado",
                    "TÃ©cnico",
                    "Maestria",
                    "Bachiller",
                    "Licenciatura",
                    "Otro",
                ]
            },
            options : {
                responsive : true,
            }
        };

       var nombramiento = {
           type: "pie",
           data : {
               datasets :[{
                   data : [
                       datos.profesores.pn_investigador,
                       datos.profesores.pn_profesor_asignatura,
                       datos.profesores.pn_profesor_carrera,
                       datos.profesores.pn_sin_nombramiento,
                       datos.profesores.pn_tecnico_academico,
                       datos.profesores.pn_ayudante_investigador,
                       datos.profesores.pn_ayudante_profesor,
                   ],
                   backgroundColor: [
                       "#8A0829",
                       "#0B173B",
                       "#B18904",
                       "#FF4000",
                       "#B40404",
                       "#81F7D8",
                       "#F7819F",
                   ],
               }],
               labels : [
                   "Investigador",
                   "Asignatura",
                   "Carrera",
                   "Sin nombramiento",
                   "TÃ©cnico acadÃ©mico",
                   "Ayte de investigador",
                   "Ayte de profesor",
               ]
           },
           options : {
               responsive : true,
           }
       };

        var canvas = document.getElementById('chart').getContext('2d');
        window.pie = new Chart(canvas, nivel);

        var canvas = document.getElementById('nombra').getContext('2d');
        window.pie = new Chart(canvas, nombramiento);
    });
})